extern crate rustc_serialize;

use rustc_serialize::json;

#[derive(RustcDecodable, Debug)]
struct MyStruct {
  field: f64,
}

fn main() {
    let json_one = "{\"field\":0.5}";
    let struct_one = json::decode::<MyStruct>(json_one);
    println!("struct one: {:?}", struct_one);

    let json_two = "{\"field\":1}";
    let struct_two = json::decode::<MyStruct>(json_two);
    println!("struct two: {:?}", struct_two);

    let json_three = "{\"field\":-1}";
    let struct_three = json::decode::<MyStruct>(json_three);
    println!("struct three: {:?}", struct_three);
}

// Output on Raspberry Pi Zero (armv6):
// struct one: Ok(MyStruct { field: 0.5 })
// struct two: Ok(MyStruct { field: 0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005 })
// struct three: Ok(MyStruct { field: NaN })

// Output on x86-64 computer:
// struct one: Ok(MyStruct { field: 0.5 })
// struct two: Ok(MyStruct { field: 1 })
// struct three: Ok(MyStruct { field: -1 })
 
